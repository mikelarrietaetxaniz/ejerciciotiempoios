//
//  ViewController.swift
//  EjercicioTiempoIOS
//
//  Created by mastermoviles on 18/10/18.
//  Copyright © 2018 EPS. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate{

    let OW_URL_BASE = "https://api.openweathermap.org/data/2.5/weather?lang=es&units=metric&appid=1adb13e22f23c3de1ca37f3be90763a9&q="
    let OW_URL_BASE_ICON = "https://openweathermap.org/img/w/"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
            textField.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var Label: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBAction func Button(_ sender: Any) {
        let textFieldString = textField.text
        consultarTiempo(localidad: textFieldString!)
    }
    
    func consultarTiempo(localidad:String){
        let urlString = OW_URL_BASE+localidad
        let url = URL(string:urlString)
        let dataTask = URLSession.shared.dataTask(with: url!){
            datos, respuesta, error in
            // El error se trata en este mismo bloque do/catch. Se podria propagar, utilizando throws.
            do {
                let jsonStd = try JSONSerialization.jsonObject(with: datos!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String:AnyObject]
                let weather = jsonStd["weather"]! as! [AnyObject]
                let currentWeather = weather[0] as! [String:AnyObject]
                let descripcion = currentWeather["description"]! as! String
                print("El tiempo en \(localidad) es: \(descripcion)")
                //Estamos bajándonos la imagen pero todavía no la usamos
                let icono = currentWeather["icon"]! as! String
                if let urlIcono = URL(string: self.OW_URL_BASE_ICON+icono+".png" ) {
                    let datosIcono = try Data(contentsOf: urlIcono)
                    let imagenIcono = UIImage(data: datosIcono)
                    // Cola de operaciones principal, tocando la UI.
                    OperationQueue.main.addOperation() {
                        self.imageView.image = imagenIcono
                        self.Label.text = descripcion
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
                }
                
            }
            catch {
                print("Error en la llamada HTTP.")
            }
            
        }
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        dataTask.resume()
    }
    
    // UITextField Delegates
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //print(string.toInt())
        let int:Int? = Int(string)
        if int == nil{
            return true;
        }else{
            return false;
        }
    }
    
}

